﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            int countOfNotes = 3;
            ToDoNotes notes = CreateNotes(countOfNotes);
            notes.PrintNotes();
            notes.MakeMostImportantNotes();
            notes.PrintNotes();
        }

        private static ToDoNotes CreateNotes(int size)
        {
            ToDoNotes notes = new ToDoNotes();
            DateTime currentTime = DateTime.Now;
            DateTime dateTime = DateTime.FromFileTime(currentTime.ToFileTime());
            for (int i = 0; i < size; i++)
            {
                TimeNote note = new TimeNote("Stjepan Kekelic", dateTime);
                note.setLevelOfImportance(int.Parse(Console.ReadLine()));
                note.setText(Console.ReadLine());
                notes.AddNote(note);
            }
            return notes;

        }
    }
}
