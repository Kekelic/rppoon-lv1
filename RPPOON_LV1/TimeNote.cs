﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class TimeNote: Note
    {
        private DateTime creationTime;

        public TimeNote(string text, string author, int levelOfIportance, DateTime creationTime)
            : base(text, author, levelOfIportance)
        {
            this.creationTime = creationTime;
        }
        public TimeNote(string text, string author, DateTime creationTime)
            : base(text, author)
        {
            this.creationTime = creationTime;
        }
        public TimeNote(string author, DateTime creationTime)
            : base(author)
        {
            this.creationTime = creationTime;
        }
        public DateTime CreationTime
        {
            get { return this.creationTime; }
            set { this.creationTime = value; }
        }

        public override string ToString()
        {
            return base.ToString() + "\n" + this.creationTime;
        }

    }
}
