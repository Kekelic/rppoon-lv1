﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class Note
    {
        private String text;
        private String author;
        private int levelOfImportance;

        public Note(string text, string author, int levelOfImportance)
        {
            this.text = text;
            this.author = author;
            this.levelOfImportance = levelOfImportance;
        }
        public Note(string text, string author)
        {
            this.text = text;
            this.author = author;
        }
        public Note(string author)
        {
            this.author = author;
        }
        public string getText() { return this.text; }
        public string getAuthor() { return this.author; }
        public int getLevelOfImportance() { return this.levelOfImportance; }
        public void setText(string text) { this.text = text; }
        public void setLevelOfImportance(int levelOfImportance) { this.levelOfImportance = levelOfImportance; }
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            set { this.author = value; }
        }
        public int LevelOfImportance
        {
            get { return this.levelOfImportance; }
            set { this.levelOfImportance = value; }
        }

        public override string ToString()
        {
            return this.author + ", " + this.levelOfImportance + "\n" + this.text;
        }

    }
}
