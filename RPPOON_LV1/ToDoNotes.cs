﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class ToDoNotes
    {
        private List<TimeNote> notes;

        public ToDoNotes()
        {
            this.notes = new List<TimeNote>();
        }

        public void AddNote(TimeNote newNote)
        {
            notes.Add(newNote);
        }

        public void DeleteNote(TimeNote doneNote)
        {
            this.notes.Remove(doneNote);
        }


        public void PrintNotes()
        {
            foreach (TimeNote note in this.notes)
            {
                Console.WriteLine(note.ToString());
            }
        }

        public void MakeMostImportantNotes()
        {
            for (int i = this.notes.Count() - 1; i >= 0; i--)
            {
                if (this.notes[i].getLevelOfImportance() == 5)
                {
                    this.notes.RemoveAt(i);
                }
            }
        }

    }
}
